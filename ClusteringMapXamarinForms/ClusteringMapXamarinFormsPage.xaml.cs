﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ClusteringMapsXamarinForms;
using ClusteringMapXamarinForms.CustomFormElements;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
namespace ClusteringMapXamarinForms
{
	public partial class ClusteringMapXamarinFormsPage : ModalHostPage
	{
		public CustomMapForAndroid customMapAndroid { get; set; }
		//public Grid Main = new Grid();

		public ClusteringMapXamarinFormsPage()
		{

			if (Device.OS == TargetPlatform.iOS)
			{
				InitializeComponent();
				CustomMap myMap = new CustomMap();
				myMain.Content= myMap;
			}
			else
			{
				InitializeComponent();

				Data Data = new Data();
				customMapAndroid = new CustomMapForAndroid()
				{
					MapType = MapType.Street,
					WidthRequest = App.ScreenWidh,
					HeightRequest = App.ScreenHeigh,
					Parameter = 0
				};
				customMapAndroid.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(36.83543436404748, 10.238987484454494), Distance.FromMiles(1.0)));
				customMapAndroid.CustomPins = Data.Items.ToList();
				//foreach (var item in customMap.CustomPins)
				//{
				//	customMap.Pins.Add(item.Pin);
				//}
				//Main.Children.Add(customMapAndroid);
				myMain.Content = customMapAndroid;



			}


		}
	}

}