﻿using System;
using System.Collections.Generic;
using ClusteringMapXamarinForms.CustomFormElements;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
namespace ClusteringMapXamarinForms
{
	public class CustomMap : Map
	{
		public List<CustomPin> CustomPins { get; set; }
		public List<Position> RouteCoordinates { get; set; }
		public Position pos;
		public CustomMap()
		{
			RouteCoordinates = new List<Position>();
			CustomPins = new List<CustomPin>();
			pos = new Position();

		}
        }
}
