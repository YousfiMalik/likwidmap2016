﻿using System;
using ClusteringMapXamarinForms;
using ClusteringMapXamarinForms.iOS;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.GoogleMaps.iOS;
using Google.Maps;
using Xamarin.Forms.Maps;
using UIKit;
using System.Collections.Generic;
using GMCluster;
using Foundation;
using CoreGraphics;
using CoreAnimation;
using System.Drawing;
using ClusteringMapXamarinForms.CustomFormElements;
using CoreFoundation;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace ClusteringMapXamarinForms.iOS
{
	public class CustomMapRenderer : MapRenderer, IGMUClusterManagerDelegate, IMapViewDelegate

	{
		const int kClusterItemCount = 250;
		const double kCameraLatitude = 41.894708;
		const double kCameraLongitude = 12.492523;
		const double extent = 0.2;
		MapView map=new MapView();
		public int i = 0;
		public static Details dtPage = null;
		GMUClusterManager clusterManager;

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{

			base.OnElementChanged(e);
			if (e.OldElement != null)
			{
				map = Control as MapView;
				map.UserInteractionEnabled = true;
				AddCluster();
				var camera = CameraPosition.FromCamera(kCameraLatitude, kCameraLongitude, 20);
				map = MapView.FromCamera(CGRect.Empty, camera);
				map.MyLocationEnabled = true;
			}

			if (e.NewElement != null)
			{
				
				var formsMap = (CustomMap)e.NewElement;
				map = Control as MapView;
			
				AddCluster();
				var camera = CameraPosition.FromCamera(kCameraLatitude, kCameraLongitude, 20);
				map = MapView.FromCamera(CGRect.Empty, camera);
				map.MyLocationEnabled = true;		
		}
			}

		public UIImage ResizeImage(UIImage sourceImage, float width, float height)
		{
			UIGraphics.BeginImageContext(new SizeF(width, height));
			sourceImage.Draw(new RectangleF(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

		void AddCluster()
		{
			var googleMapView = map; //use the real mapview init'd somewhere else instead of this
			var iconGenerator = new GMUDefaultClusterIconGenerator();
			var algorithm = new GMUNonHierarchicalDistanceBasedAlgorithm();
			var renderer = new GMUDefaultClusterRenderer(googleMapView, iconGenerator);

			renderer.WeakDelegate = this;

			clusterManager = new GMUClusterManager(googleMapView, algorithm, renderer);

			for (int j = 1; j <= 10; j++)
			{
				var lat = kCameraLatitude + extent * GetRandomNumber(-1.0, 1.0);

				var lng = kCameraLongitude + extent * GetRandomNumber(-1.0, 1.0);

				var name = $"Item {j}";
				IGMUClusterItem item;

				if (j % 2 == 0)
					item = new DropInMap(j, "Photo", (float)lat, (float)lng, "pic1", "race", DateTime.Now);
				else

					item = new DropInMap(j, "Challenge", (float)lat + 0.2f, (float)lng + 0.2f, "pic2", "Challenge", DateTime.Now);

				//IGMUClusterItem item = new DropInMap(lat, lng, name);
				clusterManager.AddItem(item);

			}

			clusterManager.Cluster();

			clusterManager.SetDelegate(this, this);
		}

		public double GetRandomNumber(double minimum, double maximum)
		{
			Random random = new Random();
			return random.NextDouble() * (maximum - minimum) + minimum;
		}

		public int GetRandomNumberPic(int minimum, int maximum)
		{
			Random random = new Random();
			return random.Next() * (maximum - minimum) + minimum;
		}

		[Export("renderer:willRenderMarker:")]
		public void WillRenderMarker(GMUClusterRenderer renderer, Overlay marker)
		{
			if (marker is Marker)
			{
				var myMarker = (Marker)marker;
			//	myMarker.Icon = UIImage.FromBundle("clustermarker"); Set Icon ClusterMakrer on cluster
				if (myMarker.UserData is DropInMap)
				{
					
					DropInMap myDrop = (DropInMap)myMarker.UserData;
					DropViewOnMap mdvc = new DropViewOnMap(myDrop.id, myDrop.type, myDrop.thumbnail, myDrop.emoji);

					myMarker.IconView = new UIView();
				
					myMarker.IconView = mdvc.getView();

				}

			}

		}

		[Export("mapViewDidStartTileRendering:")]
		public bool mapViewDidStartTileRendering(MapView mapView)
		{

			map = mapView;

			return true;
		}

		[Export("mapView:didTapMarker:")]
		public bool TappedMarker(MapView mapView, Marker marker)
		{

			dtPage = new Details();
			DropInMap myDropView = (DropInMap)marker.UserData;
			dtPage.setImage(myDropView.thumbnail);
			Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(dtPage);
			return true;
		}


		[Export("mapView:didBeginDraggingMarker:")]
		public void didBeginDraggingMarker(MapView mapView, Marker marker)
		{
			if (marker is Marker)
			{
				var myMarker = (Marker)marker;
				myMarker.Icon = UIImage.FromBundle("clustermarker");
			}
		}

		[Export("clusterManager:didTapCluster:")]
		public void DidTapCluster(GMUClusterManager clusterManager, IGMUCluster cluster)
		{
			var newCamera = CameraPosition.FromCamera(cluster.Position, map.Camera.Zoom + 1);

			var update = CameraUpdate.SetCamera(newCamera);
			clusterManager.Cluster();
			map.MoveCamera(update);
		}

		[Export("renderClusters:")]
		void RenderClusters(IGMUCluster[] clusters)
		{

		}

	}
}
