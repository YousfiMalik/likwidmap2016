﻿using System;
using ClusteringMapXamarinForms;
using ClusteringMapXamarinForms.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRendererAttribute(typeof(myDrop), typeof(DropRenderer))]
namespace ClusteringMapXamarinForms.iOS
{
	public class DropRenderer : PageRenderer
	{
		
		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);
			UIAlertView alert = new UIAlertView()
			{
				Title = "alert title",
				Message = "nil",
			};
			alert.AddButton("OK");
			alert.Show();
		}
	}
}
