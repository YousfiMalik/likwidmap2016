﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using System.Reflection;
using CoreFoundation;
using ClusteringMapXamarinForms.CustomFormElements;
using ClusteringMapXamarinForms.iOS;

[assembly: ExportRendererAttribute (typeof(ModalHostPage), typeof(ModalHostPageRenderer))]
namespace ClusteringMapXamarinForms.iOS
{
    public class ModalHostPageRenderer: PageRenderer
    {
		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			if (e.OldElement as ModalHostPage != null)
			{
				var hostPage = (ModalHostPage)e.OldElement;
			
			}

			if (e.NewElement as ModalHostPage != null)
			{
				var hostPage = (ModalHostPage)e.NewElement;
		
			}

		}

      
    }
}

