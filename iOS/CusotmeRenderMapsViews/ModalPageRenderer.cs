﻿using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using CoreFoundation;
using ClusteringMapXamarinForms.CustomFormElements;
using ClusteringMapXamarinForms.iOS;

[assembly: ExportRenderer (typeof(ModalPage), typeof(ModalPageRenderer))]
namespace ClusteringMapXamarinForms.iOS
{
    public class ModalPageRenderer : PageRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

           
			this.View.BackgroundColor = Color.Transparent.ToUIColor();
			this.ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
			this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;

           
            if(e.OldElement as ModalPage != null)
            {
                var hostPage = (ModalPage)e.OldElement;
                hostPage.CloseModalRequested -= OnCloseRequested;

            }

            if (e.NewElement as ModalPage != null)
            {
                var hostPage = (ModalPage)e.NewElement;
                hostPage.CloseModalRequested += OnCloseRequested;

            }
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            SetElementSize (new Size (View.Bounds.Width, View.Bounds.Height));
        }


        static async void OnCloseRequested(object sender, ModalPage.CloseModalRequestedEventArgs e)
        {
			if (CustomMapRenderer.dtPage!=null)
			await Xamarin.Forms.Application.Current.MainPage.Navigation.PopModalAsync(true);
        }

	
	}


}

