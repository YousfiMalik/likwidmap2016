﻿using System;

using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace ClusteringMapXamarinForms.iOS
{
	public partial class DropViewOnMap : UIViewController
	{
		public DropViewOnMap() : base("DropViewOnMap", null)
		{
		}

		public DropViewOnMap(int id, String type, String thumbnail, String emoji) : base("DropViewOnMap", null)
		{
			base.LoadView();
			//this.View.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width-650f, UIScreen.MainScreen.Bounds.Height-800f);
			this.View.BackgroundColor = Color.Transparent.ToUIColor();
			this.thumbnail.Image = UIImage.FromBundle(thumbnail);
			//this.thumbnail.Layer.CornerRadius = -12;
			this.thumbnail.BackgroundColor = Color.Transparent.ToUIColor();
			this.emoji.BackgroundColor = Color.Transparent.ToUIColor();
			this.emoji.Image = UIImage.FromBundle(emoji);
			//UIView myView = new UIView();
			if (type.Equals("Challenge"))
			{
				//this.View.BackgroundColor = Color.Transparent.ToUIColor();
				this.description.BackgroundColor = Color.FromRgb(42, 44, 51).ToUIColor();
				this.description.TextColor = Color.White.ToUIColor();
				this.description.Text = "SICK TRICK CONTEST";
			}
			else
			{
				this.description.RemoveFromSuperview();
			}
		}

		public UIView getView()
		{
			return this.View;
		}


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

